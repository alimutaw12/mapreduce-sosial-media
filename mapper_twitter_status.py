#!/home/bigdata/anaconda3/bin/python
import json
import sys
from dateutil import parser

for line in sys.stdin:
	datas = json.loads(line)
	for data in datas:
		createdAt = parser.parse(data['created_at']).date()
		print("%s\t%s" % (createdAt, 1))
