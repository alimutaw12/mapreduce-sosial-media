#!/home/bigdata/anaconda3/bin/python
import json
import sys
from dateutil import parser

for line in sys.stdin:
	datas = json.loads(line)
	for data in datas:
		kind, type = data['kind'].strip().split("#")
		if (type == 'comment'):
			date = parser.parse(data['snippet']['publishedAt']).date()
			print("%s\t%s" % (date, 1))
