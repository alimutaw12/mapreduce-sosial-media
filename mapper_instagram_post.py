#!/home/bigdata/anaconda3/bin/python
import json
import sys
from datetime import datetime

for line in sys.stdin:
	datas = json.loads(line)
	date = datetime.fromtimestamp(int(datas[0]['created_time'])).date()
	print("%s\t%s" % (date, 1))
