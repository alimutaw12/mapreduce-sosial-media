#!/home/bigdata/anaconda3/bin/python
import json
import sys
from datetime import datetime

for line in sys.stdin:
	datas = json.loads(line)
	for data in datas:
		date = datetime.fromtimestamp(int(data['created_time'])).date()
		print("%s\t%s" % (date, 1))
