#!/home/bigdata/anaconda3/bin/python
import json
import sys
from dateutil import parser

for line in sys.stdin:
	datas = json.loads(line)
	date = parser.parse(datas[0]['snippet']['publishedAt']).date()
	print("%s\t%s" % (date, 1))
