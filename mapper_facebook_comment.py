#!/home/bigdata/anaconda3/bin/python
import json
import sys
from dateutil import parser

for line in sys.stdin:
	datas = json.loads(line)
	for data in datas:
		comments = data['comments']['data']
		for comment in comments:
			createdAt = parser.parse(comment['created_time']).date()
			print("%s\t%s" % (createdAt, 1))
